1.  Cloner le projet
2.  Se déplacer dans la racine du projet puis lancer un composer update pour récuperer toutes les dependances du projet
4.  Vérifier que la base de données est sur le bon port (pour ce projet la bdd est configurée sur le port 3306) c'est le port est modifiable dans le fichier .env
5.  Lancer le serveur(php bin/console serveur:run)
6.  Lancer npm/yarn install pour récuper les depences des assets
7.  Lancer npm/yarn run dev-server pour démarrer le serveur en mode dev pour les assets
8.  Lancer php bin/console doctrine:fixtures:load pour alimenter la base de données avec un ficher de fixtures
9.  Lancer sur le navigateur l URL: http://127.0.0.1:8000/ pour visualiser le site.