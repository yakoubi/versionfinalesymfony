<?php

namespace App\DataFixtures;

use App\Entity\Projets;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $mesProjets = array(
            "JavaEE"     => array( "description" => "C'est une application de listing et de mises à jour de monuments de villes françaises. Langage de programmation et outils utilisés: JEE, Hibernate, MySQL, Spring.",
            "lien" => "https://gitlab.com/yakoubi/projetjee"),
            "TER"        => array("description" => "Création d’un générateur de sites web basé sur l’utilisation de la bibliothèque OpenLayers. Les sites web générés permettent de visualiser des « bureaux » géolocalisés sur une carte.",
            "lien" => "https://gitlab.com/yakoubi/projetIpsM1"),
            "Angular"    => array("description" => "Ce projet consiste à développer une  application web e-commerce reposant sur l’architecture MEAN Langage de programmation et outils utilisés: Angular 8, node.js, MongoDB, Bootstrap. ",
            "lien" => "https://gitlab.com/yakoubi/projetangular"),
            "Symfony"    => array("description" => "Création d’un site web de présentation de profil qui sera utile dans la vie professionnelle. Langage de programmation et outils utilisés: Symfony 4, MySQL, PHP7, Composer. ",
            "lien" => "https://gitlab.com/yakoubi/projetsymfony"),
            "ResponsiveSkeleton" => array("description" => "Ce projet consiste à présenter des sujets de thèse pour d'eventuels futures thésards en manque d'inspiration",
            "lien" => "https://gitlab.com/yakoubi/responsiveskeleton"),
            "Pagiation"  => array("description" => "test pour visualiser la mise ne place de la pagination", "lien" => "")
        );

        foreach ($mesProjets as $monProjet=> $value) {
            $projet = new Projets();
            $projet->setNom($monProjet);
            $projet->setDescription($value['description']);
            $projet->setLien($value['lien']);
            $manager->persist($projet);

        }
        $manager->flush();
    }


}
