<?php

namespace App\Controller;


use App\Entity\Projets;
use App\Repository\ProjetsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjetController extends AbstractController
{
    /**
     * @var ProjetsRepository
     */
    private $repositroy;

    //injesction au niveau du constructeur pour communiquer avec repo (bdd)
    public function __construct(ProjetsRepository $repository)
    {
        $this->repositroy = $repository;
    }

    /**
     * @Route("/projets", name="projets.index")
     * @return Response
     */
    // injecter le composant request et PaginatorInterface directemet dans l'action index
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $mesProjet = $this->repositroy->findAll();
        $mesProjet = $paginator->paginate(
            $mesProjet,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 5)

        );

        return $this->render('projets/index.html.twig', [
            'mesProjets' => $mesProjet
        ]);
    }

    /**
     * @Route("/projets/{slug}-{id}", name="projets.show", requirements={"slug": "[a-z0-9\-]*"})
     * @param Projets $projets
     * @return Response
     */
    public function show(Projets $projets, string $slug): Response
    {
        if ($projets->getSlug() !== $slug) {
            return $this->redirectToRoute('projets.show',[
                'id' => $projets->getId(),
                'slug' =>$projets->getSlug()
            ], 301);
        }
        return $this->render('projets/show.html.twig', [
            'projet' =>$projets
        ]);
    }

}