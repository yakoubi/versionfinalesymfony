<?php

namespace  App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @return Respons
     */
// ici on dit le type de retour c Response qui est la page html mentionnée ci-dessous
    public function index(): Response
    {
        return $this->render('pages/home.html.twig');
    }
}

