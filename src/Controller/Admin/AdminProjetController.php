<?php
namespace App\Controller\Admin;

use App\Entity\Projets;
use App\Form\ProjetType;
use App\Repository\ProjetsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminProjetController extends AbstractController
{
    /**
     * @var ProjetsRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private  $em;

    public function __construct(ProjetsRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin", name="admin.projet.index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $projets = $this->repository->findAll();

        $projets = $paginator->paginate(
            $projets,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 5)

        );

        return $this->render('admin/projets/index.html.twig', compact('projets'));
    }

    /**
     * @Route("/admin/projet/add", name="admin.projet.add")
     */
    public function add(Request $request)
    {
        $projet = new Projets();
        $form = $this->createForm(ProjetType::class, $projet);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($projet);
            $this->em->flush();
            return $this->redirectToRoute('admin.projet.index');
        }
        return $this->render('admin/projets/add.html.twig',[
            'projet' =>$projet,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/admin/projet/{id}", name="admin.projet.edit", methods="GET|POST")
     * @param Projets $projets
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Projets $projets, Request $request)
    {
        $form = $this->createForm(ProjetType::class, $projets);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            return $this->redirectToRoute('admin.projet.index');
        }
        return $this->render('admin/projets/edit.html.twig',[
            'projets' =>$projets,
            'form' => $form->createView()
        ]);
    }

    /**
     *  @Route("/admin/projet/{id}", name="admin.projet.delete", methods="DELETE")
     * @param Projets $projets
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Projets $projets)
    {
       $this->em->remove($projets);
       $this->em->flush();
       return $this->redirectToRoute('admin.projet.index');
    }
}
